'''
Compare n=10,100,1000 w/ respective analytical solutions.
'''

from scitools.std import *
import sys

#setup
n = 0;
#input data file you want to plot as numerical solution
f = open('table1d.dat','r')
n = int(f.readline())
x1 = zeros(n+2);
v1 = zeros(n+2);

#boundary conditions
x1[0] = 0
x1[n+1] = 1
v1[0] = 0
v1[n+1] = 0

#read
for i in range(n):
	x1[i+1] = float(f.readline())
	v1[i+1] = float(f.readline())
f.close()

#analytical solution
def sol(x):
	return 1. - (1. - exp(-10.))*x - exp(-10*x)


#PLOT ALL
plot(x1,v1, 'xb')
hold('on')
plot(x1,sol(x1), '-r')


xlabel('x')
ylabel('v')
legend('Numerical Solution', 'Analytical Solution')
hardcopy('testplot.eps')


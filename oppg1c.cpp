#include <iostream>
#include <armadillo>


using namespace std;
using namespace arma;

//function to find relative error 
//u <- analytical, v <- numerical
double fError(double u,double v) {
	return log10(abs((v-u)/u));
}

double getMaxError(int m) {
	//set up
	int n = m;
	double h = 1./(n+1.); 
	vec b = 2.*ones<vec>(n);
	//double a = -1.; //a '=' vec a = vec c
	vec x(n);
	for (int i = 0; i < n; i++) {
		x[i] = i+1;
	}
	x = x*h;
	vec f = pow(h,2)*100.*exp(-10.*x); // forcing function
	vec v(n); //put solutions here

	//Forward subst
	for (int i = 2; i <= n; i++) {
		b[i-1] = b[i-1] - 1./b[i-2]; 
		// could the mult in previous line be placed outside loop for efficiency?
		f[i-1] = f[i-1] + f[i-2]/b[i-2]; 
	}
	
	//Backward and solve
	v[n-1] = f[n-1]/b[n-1];
	int j = n-2;
	while (j > -1) {
		v[j] = (f[j] + v[j+1])/b[j];
		j--;
	}

	//Solve analytical and put into va
	vec u(n);
	for (int i = 0; i<n; i++) {
		u[i] = 1-(1-exp(-10))*x[i] - exp(-10*x[i]);
	}  

	//Trail vector v, find largest relative error
	double error = 0;
	double temp;
	for (int i = 0; i<n; i++) {
		temp = fError(u[i],v[i]);
		if (abs(error) < abs(temp)) 
			error = temp;
	}
	return error;
}

int main()
{
	
	//max errors from n = 10 to about n = 10^5
	//steps of 1000
	vec xe(100);
	vec ye(100);
	ofstream outfile;
	outfile.open("probctable.dat");
	outfile << "Max errors from n = 10 to about n = 10^5,steps of 1000" << endl;
	outfile << "log(h) \t" << "max log((v-u)/u)" << endl;
	xe[0] = log10(1./(11));
	ye[0] = getMaxError(10);
	outfile << xe[0] << "\t" << ye[0] << endl;
	for (int j = 1; j < 100; j++)	{
		xe[j] = log10(1./(j*1000+1.));
		ye[j] = getMaxError(j*1000+1);
		outfile << xe[j] << "\t" << ye[j] << endl;
	}

	outfile.close();
	

	return 0;
}

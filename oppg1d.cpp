#include <iostream>
#include <fstream>
#include <armadillo>
#include "time.h"
#include <stdio.h>
// lu(A)
// solve(U, solve(L,b)) 

using namespace std;
using namespace arma;
double LU(int m)
{
	//set up matrices and vectors
	int n = m;
	double h = 1./(n+1.); 
	mat A = zeros<mat>(n,n);
	vec x(n);
	for (int i = 0; i < n; i++) {
		x[i] = i+1.;
		A(i,i) = 2.;
		if (i<n-1)
			A(i,i+1) = -1.;
		if (i>0)
			A(i,i-1) = -1.;		
	}
	x = x*h;
	vec f = pow(h,2)*100.*exp(-10.*x); // forcing function
	vec v(n); //put solutions here
	//LU decompose A, solve with armadillo
	//timed for n = 10, 100, 1000
	mat L,U;
	//start timer
	clock_t start, finish;  //  declare start and final time
    start = clock();
	lu(L,U,A);
	v = solve(trimatu(U), solve(trimatl(L), f));
	//end timer
    finish = clock();
    double time = ( (1.*finish - 1.*start)/CLOCKS_PER_SEC );
	return time;
}

double trid(int m)
{
	//set up
	int n = m;
	double h = 1./(n+1.); 
	vec b = 2.*ones<vec>(n);
	vec x(n);
	for (int i = 0; i < n; i++) {
		x[i] = i+1;
	}
	x = x*h;
	vec f = pow(h,2)*100.*exp(-10.*x); // forcing function
	vec v(n); //put solutions here

	//start timer
	clock_t start, finish;  //  declare start and final time
    start = clock();
	//Forward subst
	for (int i = 2; i <= n; i++) {
		b[i-1] = b[i-1] - 1./b[i-2]; //a*a/b[i-2] = 1/b[i-2]
		f[i-1] = f[i-1] + f[i-2]/b[i-2]; 
	}
	
	//Backward and solve
	v[n-1] = f[n-1]/b[n-1];
	int j = n-2;
	while (j > -1) {
		v[j] = (f[j] + v[j+1])/b[j];
		j--;
	}
	//end timer
    finish = clock();
    double time = ( (1.*finish - 1.*start)/CLOCKS_PER_SEC );
	ofstream outfile;
	outfile.open("table1d.dat");
	outfile << n << endl;
	for (int i = 0; i < n; i++) {
		outfile << x[i] << endl; 
		outfile << v[i] << endl;
	}
	outfile.close();
	return time;
}

int main()
{
	int n1 = 10;
	int n2 = 100;
	int n3 = 1000;
	vec timepassed(6);
	timepassed[0] = LU(n1);
	timepassed[1] = trid(n1);
	timepassed[2] = LU(n2);
	timepassed[3] = trid(n2);
	timepassed[4] = LU(n3);
	timepassed[5] = trid(n3);

	//output to table
	ofstream outfile;
	outfile.open("table1d.dat");
	outfile << "matrix size: " << "Time LU: " << "Time tridiagonal: " << endl; 
	outfile << n1 << "\t\t" << timepassed[0] << "\t\t" << timepassed[1] << endl;
	outfile << n2 << "\t\t" << timepassed[2] << "\t\t" << timepassed[3] << endl;
	outfile << n3 << "\t" << timepassed[4] << "\t" << timepassed[5] << endl;
	outfile.close();

	return 0;
}

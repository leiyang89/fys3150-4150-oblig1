#include <iostream>
#include <fstream>
#include <armadillo>
#include "time.h"


using namespace std;
using namespace arma;

int main()
{
	clock_t start, finish;
	//set up matrices
	int n = pow(10,3);

	mat B = randu<mat>(n,n);
	mat C = randu<mat>(n,n);
	mat A = zeros<mat>(n,n);
	
	start = clock();
	// std row-major
	for (int i=0; i < n; i++) {
		for (int j=0; j < n; j++) {
			for(int k=0; k < n; k++) {
				A(i,j) = B(i,k)*C(k,j);
			}
		}
	}
	finish = clock();
	cout << ((1.*finish - 1.*start)/CLOCKS_PER_SEC) << endl;

	// column-major
	A = zeros<mat>(n,n);
	start = clock();
	for(int j=0; j < n; j++) {
		for(int i=0; i<n; i++) {
			for(int k=0; k<n; k++) {
				A(i,j) += B(i,k)*C(k,j);
			}
		}
	}	
	finish = clock();
	cout << ((1.*finish - 1.*start)/CLOCKS_PER_SEC) << endl;
	// armadillo
	A = zeros<mat>(n,n);
	start = clock();
	A = B*C; 
	finish = clock();
	cout << ((1.*finish - 1.*start)/CLOCKS_PER_SEC) << endl;

	return 0;
}

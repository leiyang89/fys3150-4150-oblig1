#include <iostream>
#include <fstream>
#include <armadillo>


using namespace std;
using namespace arma;

int main()
{
	//set up
	int n = 100;
	double h = 1./(n+1.); 
	vec b = 2.*ones<vec>(n);
	double a = -1.; //a '=' vec a = vec c
	vec x(n);
	for (int i = 0; i < n; i++) {
		x[i] = i+1;
	}
	x = x*h;
	vec f = pow(h,2)*100.*exp(-10.*x); // forcing function
	vec v(n); //put solutions here

	//Forward subst
	for (int i = 2; i <= n; i++) {
		b[i-1] = b[i-1] - 1./b[i-2]; // - a*c/b[i-2] = - 1./b[i-2]
		f[i-1] = f[i-1] + f[i-2]/b[i-2]; 
	}
	
	//Backward and solve
	v[n-1] = f[n-1]/b[n-1];
	int j = n-2;
	while (j > -1) {
		v[j] = (f[j] - a*v[j+1])/b[j];
		j--;
	}

	//Write to file to plot
	//output n, x, v
	ofstream outfile;
	outfile.open("plot1b100.dat");
	outfile << n << endl;
	for (int i = 0; i < n; i++) {
		outfile << x[i] << endl; 
		outfile << v[i] << endl;
	}
	outfile.close();

	return 0;
}
